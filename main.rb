require_relative 'url_storage'
require_relative 'crawler'

urls = UrlStorage.new.urls
crawler = Crawler.new(urls)
crawler.crawl
