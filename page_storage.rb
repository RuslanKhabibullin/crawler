require_relative 'background_saver'

class PageStorage
  
  def initialize
    @insertable_collection = []
  end

  def collect_and_save(data)
    @insertable_collection << data
    return unless @insertable_collection.size < 20
    BackgroundSaver.perform_async(@insertable_collection)
    @insertable_collection.clear
  end
end
