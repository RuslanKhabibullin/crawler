require_relative 'url_utils'
include UrlUtils

module HtmlUtils
  def page_title(html_page)
    html_page.css('h1').text
  end

  def page_clean(html_page)
    html_page.css("style,script").remove
    html_page.text.gsub(/(\t|\n)/, '')
  end

  def urls_from_page(html_page, url)
    hrefs = []
    links = html_page.css('a')
    links.each do |link|
      href = link.attribute('href').to_s
      if UrlUtils.valid?(href)
        href = UrlUtils.make_absolute(url, href) if UrlUtils.relative?(href)
        hrefs << href
      end
    end
    hrefs.uniq!
  end
end
