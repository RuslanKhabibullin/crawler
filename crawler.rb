require 'java'
java_import 'java.util.concurrent.Callable'
java_import 'java.util.concurrent.FutureTask'
java_import 'java.util.concurrent.LinkedBlockingQueue'
java_import 'java.util.concurrent.ThreadPoolExecutor'
java_import 'java.util.concurrent.TimeUnit'
require "march_hare"
require_relative 'thread_crawl_body'
require_relative 'urls_adder'

class Crawler
  CONSUMER_COUNT = 3
  @@urls = []

  def initialize(urls)
    @connection = MarchHare.connect
    @channel = @connection.create_channel
    @exchange = @channel.topic("urls_parser", :auto_delete => true)
    @@urls = urls
  end

  def self.add_urls(urls)
    @@urls.concat(urls)
    @@urls.uniq!
  end

  def self.urls
    @@urls
  end

  def crawl
    listen
    while true do
      threads_crawl
    end
  end

  private
  def threads_crawl
    incrementer = 0
    while @@urls.length > 0
      urls_to_fetch = @@urls.length > 100 ? @@urls.shift(100) : @@urls.shift(@@urls.length)
      incrementer = 0 if incrementer > CONSUMER_COUNT - 1
      @exchange.publish(Marshal.dump(urls_to_fetch), routing_key: "urls_parser.#{incrementer}")
      incrementer += 1
      sleep(0.6)
    end
  end

  def listen
    pool = MarchHare::ThreadPools.fixed_of_size(20)
    CONSUMER_COUNT.times do |i|
      q = @channel.queue("", :exclusive => true)
      q.bind(@exchange, :routing_key => "urls_parser.#{i}")
      q.subscribe(:blocking => false) do |metadata, payload|
        pool.submit do
          finded_urls = crawl_batch(Marshal.load(payload))
          UrlsAdder.perform_async(finded_urls)
        end
      end
    end
  end

  def crawl_batch(urls)
    executor = ThreadPoolExecutor.new(4, 4, 100, TimeUnit::SECONDS, LinkedBlockingQueue.new)
    tasks = []
    while urls.length > 0
      urls_batch = urls.length > 25 ? urls.shift(25) : urls.shift(urls.length)
      thread_crawl_body = ThreadCrawlBody.new(urls_batch)
      task = FutureTask.new(thread_crawl_body)
      executor.execute(task)
      tasks << task
    end
    finded_urls = tasks.map { |t| t.get }.flatten!
    executor.shutdown
    finded_urls
  rescue => e
    p e.message
  end
end
