require 'mongo'

class DatabaseConnection
  @@client = nil

  def client
    @@client
  end

  def initialize(address: '127.0.0.1:27017', database: 'crawler-db', timeout: 10)
    unless client
      @@client = Mongo::Client.new([address],
                                   database: database,
                                   server_selection_timeout: timeout,
                                   max_pool_size: 20,
                                   min_pool_size: 5)
    end
  rescue Mongo::Error::NoServerAvailable => e
    puts e.message
  end
end
