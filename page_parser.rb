require 'nokogiri'
require 'restclient'
require_relative 'html_utils'
include HtmlUtils

class PageParser
  attr_accessor :result

  def initialize(url)
    @result = PageParser.page_data_from_url(url)
  end

  def self.page(url)
    Nokogiri::HTML(RestClient.get(url))
  end

  def self.page_data(page, url)
    title = HtmlUtils.page_title(page)
    content = HtmlUtils.page_clean(page)
    { url: url, page_title: title, page_content: content }
  end

  def self.page_data_from_url(url)
    page = PageParser.page(url)
    PageParser.page_data(page)
  end

  def self.page_urls(page, url)
    HtmlUtils.urls_from_page(page, url)
  end
end
