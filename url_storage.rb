class UrlStorage
  attr_accessor :urls

  def initialize(file = 'urls.txt')
    self.urls = read_from_file(file)
  end

  private

  def read_from_file(url_file)
    lines = []
    File.open(url_file, 'r') do |file|
      file.readlines.each do |line|
        lines << line.chomp
      end
    end
    lines
  end
end
