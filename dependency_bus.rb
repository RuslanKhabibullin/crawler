require_relative 'database_connection'

class DependencyBus
  @@dependency_bus = {}

  def self.add_dependency(key, dependency)
    @@dependency_bus[key] = dependency
  end

  def self.dependencies
    @@dependency_bus
  end

  def self.clear
    @@dependency_bus.clear
  end

  def self.exist?(key)
    @@dependency_bus.key?(key)
  end
end
