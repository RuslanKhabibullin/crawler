module UrlUtils
  def anchor?(url)
    url =~ /^#/ ? true : false
  end

  def valid?(url)
    !anchor?(url) && !url.empty?
  end

  def relative?(url)
    url =~ /^http/ ? false : true
  end

  def make_absolute(base_url, relative_url)
    base_url = remove_extra_paths(base_url)
    base_url + relative_url
  end

  def remove_extra_paths(base_url)
    index_to_start_slash_search = base_url.index('://') + 3
    index_of_first_relevant_slash = base_url.index('/', index_to_start_slash_search)
    index_of_first_relevant_slash ? base_url[0, index_of_first_relevant_slash] : base_url
  end
end
