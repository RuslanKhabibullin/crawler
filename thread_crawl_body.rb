require 'java'
java_import 'java.util.concurrent.Callable'
require_relative 'page_parser'
require_relative 'page_storage'

class ThreadCrawlBody
  include Callable

  def initialize(url_group)
    @url_group = url_group
    @page_storage = PageStorage.new
  end

  def call
    @url_group.map do |url|
      begin
        page = PageParser.page(url)
        save_page(page, url)
        PageParser.page_urls(page, url)
      rescue
        next
      end
    end
  end

  def save_page(page, url)
    page_data = PageParser.page_data(page, url)
    @page_storage.collect_and_save(page_data)
  end
end
