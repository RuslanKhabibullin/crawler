require 'sucker_punch'
require_relative 'crawler'

class UrlsAdder
  include SuckerPunch::Job
  workers 4

  def perform(urls)
    Crawler.add_urls(urls) if Crawler.urls.length < 600_000
  end
end
