require_relative 'dependency_bus'
require_relative 'database_connection'
require 'sidekiq'

class BackgroundSaver
  include Sidekiq::Worker
  sidekiq_options retry: 1

  def perform(pages)
    db_client = BackgroundSaver.connection
    db_client[:pages].insert_many(pages, ordered: false)
  rescue => e
    p e.message
  end

  def self.connection
    unless DependencyBus.exist?('db_client')
      DependencyBus.add_dependency('db_client', DatabaseConnection.new.client)
    end
    DependencyBus.dependencies['db_client']
  end
end
